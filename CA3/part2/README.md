# Class Assignment 3 Part 2

The source code for this assignment is located in the folder
[CA3-part2](https://bitbucket.org/1211777/devops-21-22-atb-1121777/src/master/CA3/part2)

## 1. Analysis, Design and Implementation

To perform the requested tasks, it is necessary to install Vagrant in order to 
create virtual boxes running in Virtual Box.

### Software used to perform this class assignment:
**Local machine:**</p>

  - Visual studio code (linked to the ubuntu into the VM via ssh for editing
    files)</p>
  - VirtualBox (To run the boxes installed with vagrant.</p>
  - Git (To download repositories into the vm)</p>
  - Vagrant
  
#### Tasks:
**Creation of a box using vagrant:**

First is necessary to create a new vagrant file.
For that, after creating a directory to this vagrant machine, it's used the command:
```bash
vagrant init envimation/ubuntu-xenial
```
After that is crated a file with the standard configuration of the box "envimation/ubuntu-xenial"

Then we need to run the command: 
```bash
vagrant up
```
and the box will start to run.


**Creation of two boxes to run the application tut-react-basic**

First is needed to download the vagrant file from the repository:
[vagrant-multi-sp ring-tut-demo](
https://bitbucket.org/atb/vagrant-multi-spring-tut-demo/src/master/)

Then is neded to update some files and configurations:

- on the vagrantfile:

```ruby
    web.vm.provider "virtualbox" do |v|
    # Increase Ram memory
      v.memory = 2048
    end
```

- change path and add a command to git (setted the repository to public): 

```ruby
      git clone https://1211777@bitbucket.org/1211777/devops-21-22-atb-1121777.git
      cd devops-21-22-atb-1121777/CA3/part2/tut-basic-gradle
```

It was necessary to edit other files, and for that was followed the commits from the following repository: [tut-basic-gradle](https://bitbucket.org/atb/tut-basic-gradle/commits/)

- After creation of the vagrantfile and the edition of other files, the machine was set to run:

```
vagrant up
```
![virtualBox](Images/virtualBox.png)

---

The first step back was:

- When reloading Git doesn't pull from the remote. </p>

![git_pull](Images/git_clone_fail.png)

So I added the the pull command to ensure that the repository is up to date every time I reload the box.
```ruby
      git clone https://1211777@bitbucket.org/1211777/devops-21-22-atb-1121777.git
      cd devops-21-22-atb-1121777
      git pull # this was added to make pull from the repository when reloading the box.
      cd CA3/part2/tut-basic-gradle
```

For make the app running I needed to downgrade the version of java from my application. 
For the CA2 I used java11, and I've got the following error: 

![error_java](Images/Error_isBlank.jpg)
So I needed to remove the method string.isBlank() and update relative tests.

---
After this I was able to got into the locations and insert values into the tables:

- web box:
http://192.168.56.10:8080/basic-0.0.1-SNAPSHOT/

![initialState](Images/initialState.jpg)

- h2 database box: 
http://192.168.56.10:8080/basic-0.0.1-SNAPSHOT/h2-console

![insert into Table](Images/insert_into_table.jpg)

The result was: 

![result_insert_into](Images/result_insert_into.jpg)

---

**Used commands during this class assignment:**
```bash
# Destroy the virtual machine, and not delete the box:
vagrant destroy -f 

#Remove the box from local machine:
vagrant box remove 'box_name'

#Reload a box configuration:
vagrant reload --provision

#Stop the box from running:
vagrant halt
```
---
## 2. Alternative
The source code for this assignment is located in the folder
[CA3-part2-Alternative](https://bitbucket.org/1211777/devops-21-22-atb-1121777/src/master/CA3/part2/Alternative)

---
## 3. Issues and tags

### Issues created

| Issue number | Class Assignment | Description                  |
|--------------|------------------|------------------------------|
| #10          | CA3 Part-2       | Virtualization with Vagrant  |

### Tag's

| Tag versions | Class Assignment | Description             |
|--------------|------------------|-------------------------|
| ca3-part2    | CA3 Part-2       | Final version CA3 Part2 |

## 4. Student identification

* **Name:** Luís Filipe Salgado Alves
* **E-mail:** 1211777@isep.ipp.pt
* **TeamGroup:** Grupo4
* **Class:** TurmaB
* **Course:** SWitCH | Re-Qualification Programme for IT

---