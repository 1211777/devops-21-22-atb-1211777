/*
 * Copyright 2015 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.greglturnquist.payroll;

import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 * @author Greg Turnquist
 */
// tag::code[]
@Entity // <1>
public class Employee {

    private @Id
    @GeneratedValue
    Long id; // <2>
    private String firstName;
    private String lastName;
    private String description;
    private String jobDescription;
    private int jobYears;
    private String email;


    private Employee() {
    }

    public Employee(String firstName, String lastName, String description,
                    String jobDescription, int jobYears, String email) {
        validationOfString(firstName);
        this.firstName = firstName;
        validationOfString(lastName);
        this.lastName = lastName;
        validationOfString(description);
        this.description = description;
        validationOfString(jobDescription);
        this.jobDescription = jobDescription;
        validationOfInteger(jobYears);
        this.jobYears = jobYears;
        isValidEmail(email);
        this.email = email;

    }

    private void validationOfString(String s) {
        if (s.isEmpty()) {
            throw new IllegalArgumentException("The string cannot be empty, " +
                    "blank or null");
        }
    }

    private void validationOfInteger(int i) {
        if (i <= 0 || i > 100) {
            throw new IllegalArgumentException("Should be a positive Integer " +
                    "between 1 and 100");
        }
    }

    private static void isValidEmail(String email) {
        if (email != null && email.length() > 0) {
            String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
            Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
            Matcher matcher = pattern.matcher(email);
            if (!matcher.matches()) {
                throw new IllegalArgumentException("The e-mail is invalid.");
            }
        }
    }
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Employee employee = (Employee) o;
        return Objects.equals(id, employee.id)
                && Objects.equals(firstName, employee.firstName)
                && Objects.equals(lastName, employee.lastName)
                && Objects.equals(description, employee.description)
                && Objects.equals(jobDescription, employee.jobDescription)
                && Objects.equals(jobYears, employee.jobYears)
                && Objects.equals(email, employee.email)
                ;


    }

    @Override
    public int hashCode() {

        return Objects.hash(id, firstName, lastName, description,
                jobDescription, jobYears, email);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getJobDescription() {
        return jobDescription;
    }

    public void setJobDescription(String jobDescription) {
        this.jobDescription = jobDescription;
    }

    public int getJobYears() {
        return jobYears;
    }

    public void setJobYears(int jobYears) {
        this.jobYears = jobYears;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
    @Override
    public String toString() {
        return "Employee{" +
                "id=" + id +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", description='" + description + '\'' +
                ", jobDescription='" + jobDescription + '\'' +
                ", jobYears='" + jobYears + '\'' +
                ", email='" + email + '\'' +
                '}';
    }
}
// end::code[]
