# Class Assignment 3 Part 2 - ALTERNATIVE


## 1. Analysis, Design and Implementation


To perform the requested tasks, it is necessary to habilitate the Hyper-V on windows to create virtual boxes. 
Note: This software is available only on windows-pro versions.

### Software used to perform this class assignment:
**Local machine:**</p>

  - Visual studio code (linked to the ubuntu into the VM via ssh for editing
    files)</p>
  - Hyper-V (To run the boxes installed with vagrant.)</p>
  - Git (To download repositories into the vm)</p>
  - Vagrant
  
#### Tasks:
**Habilitation of Hyper-V**


For habilitate the Hyper-V we can use different options. the first one and the option that I use :
- Windows Enterprise, Professional, or Education 8.1

```Bash
#Command line:
Enable-WindowsOptionalFeature -Online -FeatureName Microsoft-Hyper-V -All
```

The result is visible in:

windows > Apps and Features > Programs and Features > Turn Windows Features on or off

![Hyper-V enabling](Images/habilitate_hyperV.jpg)

---

**Creation of two boxes to run the application tut-react-basic**

First is needed to create a vagrantfile, and for this I used the same as CA3-Part2.

Then is needed to update some configurations:

- Changing the box:

I changed the box because it was suported only on the Virtual Box. The one that I chose was based on Ubuntu 18.04 similar as the one sugested.

![AlternativeBox](Images/AlternativeBox.png)

- on the vagrantfile:

```ruby
#Change all the references from the previous box to the one that I choose.
Vagrant.configure("2") do |config|
  #config.vm.box = "envimation/ubuntu-xenial" #Previous version#
  config.vm.box = "hashicorp/bionic64" #Actual version
  #Setting the provider
  config.vm.provider "hyperv"

```

- Due to vagrant limitations, it cannot enforce a static IP automatically, since it does not know how to create and configure new networks for Hyper-V. 
So we need to change the <s>spring.datasource.url</s> from ip to the machine name

```ruby
#From
spring.datasource.url=jdbc:h2:tcp://192.168.56.11:9092/./jpadb;DB_CLOSE_DELAY=-1;DB_CLOSE_ON_EXIT=FALSE
#To
spring.datasource.url=jdbc:h2:tcp://db:9092/./jpadb;DB_CLOSE_DELAY=-1;DB_CLOSE_ON_EXIT=FALSE

```

- It was also changed the path from the repository, to run the alternative.

```ruby
      # Change the following command to clone your own repository!
      git clone https://1211777@bitbucket.org/1211777/devops-21-22-atb-1121777.git
      cd devops-21-22-atb-1121777
      git pull
      cd CA3/part2/Alternative/tut-basic-gradle
```

- After creation of the vagrantfile and the edition of other files, the machine was set to run:

```
vagrant up --provision --provider=hyperv 
```

Hyper-V with the virtual boxes mounted and running.

![HyperV](Images/HyperV.png)

After this creation we can access via browser in the following links:

- Web: http://web:8080/basic-0.0.1-SNAPSHOT/

Initial state:

![dataBase](Images/Application.png)

- Database: http://db:8082/

Interface from h2 database:

![dataBase](Images/h2.png)


Now we can perform operations over the database:

  ![dataBase](Images/h2_operation.png)

The result was:

![dataBase](Images/Eddited_H2.png)

**Note:**

- In order to run the command in command line we need to assure that we have administrator permissions.
  So you need to run the terminal as administrator</p>

![permissions_administrator](Images/admin_fail.png)

- Hyper-v uses SMB to access into the system folders.
You need to know the host (administrator) login and password for the windows account or if is this the case, the local account password.

![password_SMB](Images/smb_pswd.png)
---

**Used commands during this class assignment:**
```bash
# Destroy the virtual machine, and not delete the box:
vagrant destroy -f 

#Remove the box from local machine:
vagrant box remove 'box_name'

#Reload a box configuration:
vagrant reload --provision

#Stop the box from running:
vagrant halt
```
---

## 2. Hyper-V analyses 

Microsoft Hyper-V is a Type-1 Hypervisor, it run on actual hardware, and VirtualBox is a Type-2 and run on host OS.

![HypervisorsType](Images/HypervisorsType.png)
</p>
Using Hyper-v is very similar to VirtualBox, however creating a box in a graphical environment is not so intuitive.
For its installation, the only obstacle is the availability for Windows Pro only.</p>

Setup is more difficult, due to the difference in hypervisors such as change memory RAM or disck size.</p>

In the vagrant file it is simple and the changes are not significant.
A disadvantage is the impossibility of not being able to define a static ip, and so to access these machines it is necessary to access the name of the desired machine.</p>

In this class assignment I didn't notice any significant difference in terms of performance between the two hypervisors used. However, the creation of virtual machines using the graphical interface of Hyper-V was not explored.

---
## 3. Issues and tags

Please view [CA3-part2](https://bitbucket.org/1211777/devops-21-22-atb-1121777/src/master/CA3/part2)

---
## 4. Student identification

* **Name:** Luís Filipe Salgado Alves
* **E-mail:** 1211777@isep.ipp.pt
* **TeamGroup:** Grupo4
* **Class:** TurmaB
* **Course:** SWitCH | Re-Qualification Programme for IT

---