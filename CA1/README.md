# Class Assignment 1 Report

The source code for this assignment is located in the
folder [CA1](https://bitbucket.org/1211777/devops-21-22-atb-1121777/src/master/CA1/)

## 1. Analysis, Design and Implementation

- Initial requirement:

For the following steps of the CA1 it's necessary to install Git for windows.

- Part1

1. The first steps, include creation of a repository and the addiction of the
   Tutorial assignment.
2. Creation of a Tag in the original version V1.0.0 , and the original version
   for the first assignment.
3. Addition of a new field jobYears and test the application;
4. Addition of the field jobDescription asked on first lecture and update tests;


- Part 2

1. Creation of a branch to add a new feature (field e-mail).
2. Add the unit tests for that feature.
3. Merge the branch into the master.
4. Creation of new branch for improve the last modification.
5. Update email field validation and tests.
6. merge the branch with master branch.

Issues were created to carry out the practical parts of the class assignment

## Issues created

| Issue number | Class Assignment   | Description                                                                    |
|--------------|--------------------|--------------------------------------------------------------------------------|
| #1           | test purpose       | Create folder for class assignment 1                                           |
| #2           | test purpose       | Add "Tutorial React.js and Spring Data REST Application" into a new folder CA1 |
| #3           | test purpose       | Add "Report_CA1_.md" to folder CA1                                             |
| #4           | CA1 part 1         | Class Assignment 1 - Part 1                                                    |
| #5           | CA1 part 2         | Class Assignment 1 - Part 2                                                    |

## Tag's

| Tag versions | Class Assignment | Description                                                                       |
|--------------|------------------|-----------------------------------------------------------------------------------|
| v1.0.0       |                  | Tutorial React.js and Spring Data REST Application (original application)         |
| v1.1.0       |                  | Tutorial React.js and Spring Data REST Application (original application for CA1) |
| v1.2.0       | CA1 part 1       | New feature added (Job Years field)                                               |
| v1.3.0       | CA1 part 2       | Add field e-mail                                                                  |
| v1.3.1       | CA1 part 2       | Improvement on field e-mail.                                                      |

## Git commands used

### Used in first and second part:

| git command                    | Description                                                         |
|--------------------------------|---------------------------------------------------------------------|
| git clone                      | Clone repository from remote to the local machine                   |
| git pull                       | Get updates from remote                                             |
| git merge                      | Get from remote when there's modifications different from the local |
| git commit -a -m "message"     | Stash the files to send for remote                                  |
| git push [from ] [to]          | Send files for remote                                               |
| git tag [version] -m "message" | Creation of tag                                                     |
| git push [tag version] origin  | Send the tag for the remote server                                  |
| git push --tags                | Send all tags for the remote server                                 |
| git tag -n                     | See actual tags                                                     |
| git push                       | Send files for remote                                               |
| git branch [branch-name]       | Creation of branches                                                |
| git checkout [branch-name]     | Change between branches                                             |
| git branch -l                  | See actual branches                                                 |
| git merge [branch-name]        | For merging the branch back into the master                         |

More commands were used, however I don't think they are relevant for the
execution of this assignment class.

## 2. Analysis of an Alternative

For the alternative I proposed to do it using MERCURIAL and I used a server in
HelixCoreHub. It's necessary to install TortoiseHG.

I performed the same tasks requested for the first part and managed to get
similar results. The main differences between the VCS are:

- different commands;
- More restricted hosting servers, there are many that are not open source, and
  that do not have the same characteristics as Bitbucket, such as the use of
  issues;
- Lack of online support or documentation, with Git there is much more support
  from the internet community.

For access the repository, I invited the docent of the class to join the
repository and access the principal features. It's presented also some images to
describe de general process.

## 3. Differences between Git and Mercurial

* Differences in commands

| Git                       | Mercurial                |
|---------------------------|--------------------------|
| git pull                  | hg pull                  |
| git push                  | hg push                  |
| git commit                | hg commit                |
| git branch                | hg branch                |
| git checkout [branchName] | hg checkout [branchName] |
| git checkout [branchName] | hg update                |



* Other differences

| Git                                                              | Mercurial                                                                                           |
|------------------------------------------------------------------|-----------------------------------------------------------------------------------------------------|
| It has many more commands and options, being more complex to use | more simplified commands                                                                            |
| Branches are much more intuitive and easier to use               | doesn't refer in the same way as git, once you delete the branch, the changes disappear completely. |
| Is there a staging before sending commits                        | there is no staging area                                                                            |
| Lots of web support, because git is an industry standard         | easy to learn but with many gaps for more complex projects                                          |
| it's slower                                                      | It's faster                                                                                         |
| Git supports the unlimited number of parents                     | Mercurial allows only two parents                                                                   |

## 4. Implementation of the Alternative

*Present the implementation of this class assignment using the git alternative*

[//]: # (Tive alguns problemas com a conta do helix core hub e não estou a conseguir)

[//]: # (partilhar com o professor. No entanto deixo aqui as imagens do resultado obtido)

[//]: # (com a reliazação deste Class assignment.)

* Commit lists form the Helix repository<p/>

![Commit list1](JPEG/Commits_1.jpg)<p/>
![Commit list2](JPEG/Commits_2.jpg)<p/>
![Commit list3](JPEG/Commits_3.jpg)<p/>
![Commit list4](JPEG/Commits_4.jpg)<p/>

* Example of Tortoise HG<p/>
  ![GraphicView_tortoiseHG](JPEG/img6_tortoise_graphic_view.jpg)<p/>
* Showing the branches<p/>
  ![Branch_list_on_repository](JPEG/Branch_List.jpg)<p/>
* Showing the tag list<p/>
  ![Tag_list_on_repository](JPEG/Tag_List.jpg)<p/>
* Showing the issues <p/>
  ![Issues_created_on_repository](JPEG/Issues_List.jpg)<p/>

[//]: # (![HelixCoreHub_general_View]&#40;JPEG/Helix_Genereal_view.jpg&#41;)

[//]: # (![HelixCoreHub_general_View]&#40;JPEG/Helix_Genereal_view_2.jpg&#41;)

[//]: # (![HelixCoreHub_general_View]&#40;JPEG/Helix_Genereal_view_3.jpg&#41;)

---

## 4. Student identification

* **Name:** Luís Filipe Salgado Alves
* **E-mail:** 1211777@isep.ipp.pt
* **TeamGroup:** Grupo4
* **Class:** TurmaB
* **Course:** SWitCH | Re-Qualification Programme for IT

---