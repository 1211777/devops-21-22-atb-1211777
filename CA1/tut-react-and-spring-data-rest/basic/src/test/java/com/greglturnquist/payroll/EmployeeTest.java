package com.greglturnquist.payroll;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import static org.junit.jupiter.api.Assertions.*;

class EmployeeTest {

    @ParameterizedTest
    @ValueSource(strings = {"", " ", "     ", "\t"})
    void creation_of_not_valid_String_FirstName(String str) {
        String lastNameA = "May";
        String descriptionA = "Professional Driver";
        int jobYearsA = 2;
        String jobDescription = "no Description";
        String email = "abcdefgh@abcdefgh.com";
        Exception exception = assertThrows(IllegalArgumentException.class,
                () -> {
                    new Employee(str, lastNameA,
                            descriptionA
                            , jobDescription, jobYearsA, email);
                });
        String expectedMessage = "The string cannot be empty, " +
                "blank or null";
        String actualMessage = exception.getMessage();
        //Act
        assertTrue(actualMessage.contains(expectedMessage));
    }

    @ParameterizedTest
    @ValueSource(strings = {"", " ", "     ", "\t"})
    void creation_of_not_valid_String_last_name(String str) {
        String firstNameA = "James";
        String descriptionA = "Professional Driver";
        int jobYearsA = 2;
        String jobDescription = "no Description";
        String email = "abcdefgh@abcdefgh.com";

        Exception exception = assertThrows(IllegalArgumentException.class,
                () -> {
                    new Employee(firstNameA, str,
                            descriptionA
                            , jobDescription, jobYearsA, email);
                });
        String expectedMessage = "The string cannot be empty, " +
                "blank or null";
        String actualMessage = exception.getMessage();
        //Act
        assertTrue(actualMessage.contains(expectedMessage));
    }

    @ParameterizedTest
    @ValueSource(strings = {"", " ", "     ", "\t"})
    void creation_of_not_valid_String_description(String str) {
        String firstNameA = "James";
        String lastNameA = "May";
        int jobYearsA = 2;
        String jobDescription = "no Description";
        String email = "abcdefgh@abcdefgh.com";
        Exception exception = assertThrows(IllegalArgumentException.class,
                () -> {
                    new Employee(firstNameA, lastNameA,
                            str, jobDescription, jobYearsA, email);
                });
        String expectedMessage = "The string cannot be empty, " +
                "blank or null";
        String actualMessage = exception.getMessage();
        //Act
        assertTrue(actualMessage.contains(expectedMessage));
    }

    @ParameterizedTest
    @ValueSource(strings = {"", " ", "     ", "\t"})
    void creation_of_not_valid_String_jobDescription(String str) {
        String firstNameA = "James";
        String lastNameA = "May";
        String descriptionA = "Professional Driver";
        int jobYearsA = 2;
        String email = "abcdefgh@abcdefgh.com";
        Exception exception = assertThrows(IllegalArgumentException.class,
                () -> {
                    new Employee(firstNameA, lastNameA,
                            descriptionA, str, jobYearsA, email);
                });
        String expectedMessage = "The string cannot be empty, " +
                "blank or null";
        String actualMessage = exception.getMessage();
        //Act
        assertTrue(actualMessage.contains(expectedMessage));
    }


    @ParameterizedTest
    @ValueSource(ints = {-1, 0, 101})
    void creation_of_not_valid_integer(int i) {
        String str = "test";
        Exception exception = assertThrows(IllegalArgumentException.class,
                () -> {
                    new Employee(str, str, str
                            , str, i, str);
                });

        String expectedMessage = "Should be a positive Integer between 1 and " +
                "100";
        String actualMessage = exception.getMessage();
        //Act
        assertTrue(actualMessage.contains(expectedMessage));
    }

    @ParameterizedTest
    @ValueSource(ints = {1, 10, 100})
    void creation_of_valid_integer(int i) {
        String str = "test";
        String email = "abcdefgh@abcdefgh.com";

        Employee employeeA = new Employee(str, str, str, str, i, email);
        Employee employeeB = new Employee(str, str, str, str, i, email);
        assertEquals(employeeA, employeeB);
    }

    @ParameterizedTest
    @ValueSource(strings = {"abcd@abcd.com.x.a",
            "someting@something",
            "email@email@email",
            "email.a.test@email.",
            "email.a.test@email.#"})
    void creation_of_not_valid_Email(String email) {
        String firstName = "May";
        String lastName = "May";
        String description = "Professional Driver";
        int jobYears = 2;
        String jobDescription = "no Description";
        Exception exception = assertThrows(IllegalArgumentException.class,
                () -> {
                    new Employee(firstName, lastName, description,
                            jobDescription
                            , jobYears, email);
                });
        String expectedMessage = "The e-mail is invalid.";
        String actualMessage = exception.getMessage();
        //Act
        assertTrue(actualMessage.contains(expectedMessage));
    }

    @Test
    void getJobYears_employeeA_dataBase() {
        //Arrange
        //Employee A
        String firstNameA = "James";
        String lastNameA = "May";
        String descriptionA = "Professional Driver";
        int jobYearsA = 2;
        String jobDescription = "no Description";
        String email = "abcdefgh@abcdefgh.com";

        Employee employeeA = new Employee(firstNameA, lastNameA, descriptionA
                , jobDescription, jobYearsA, email);
        //Act
        int result = employeeA.getJobYears();
        //Assert
        assertEquals(result, jobYearsA);
    }

    @Test
    void getID_employeeA() {
        //Arrange
        //Employee A
        String firstNameA = "James";
        String lastNameA = "May";
        String descriptionA = "Professional Driver";
        int jobYearsA = 2;
        Long expected = null;
        String jobDescription = "no Description";
        String email = "abcdefgh@abcdefgh.com";

        Employee employeeA = new Employee(firstNameA, lastNameA, descriptionA
                , jobDescription, jobYearsA, email);
        //Act
        Long result = employeeA.getId();
        //Assert
        assertEquals(result, expected);
    }

    @Test
    void setID_employeeA() {
        //Arrange
        //Employee A
        String firstNameA = "James";
        String lastNameA = "May";
        String descriptionA = "Professional Driver";
        int jobYearsA = 2;
        Long expected = 0L;
        String jobDescription = "no Description";
        String email = "abcdefgh@abcdefgh.com";

        Employee employeeA = new Employee(firstNameA, lastNameA, descriptionA
                , jobDescription
                , jobYearsA, email);
        //Act
        employeeA.setId(expected);
        Long result = employeeA.getId();
        System.out.println(result);
        //Assert
        assertEquals(result, expected);
    }

    @Test
    void getJobYears_employeeA() {
        //Arrange
        //Employee A
        String firstNameA = "James";
        String lastNameA = "May";
        String descriptionA = "Professional Driver";
        int jobYearsA = 2;
        String jobDescription = "no Description";
        String email = "abcdefgh@abcdefgh.com";

        Employee employeeA = new Employee(firstNameA, lastNameA, descriptionA
                , jobDescription, jobYearsA, email);
        //Act
        int result = employeeA.getJobYears();
        //Assert
        assertEquals(result, jobYearsA);
    }

    @Test
    void getJobYears_not_equals_employeeA() {
        //Arrange
        //Employee A
        String firstNameA = "James";
        String lastNameA = "May";
        String descriptionA = "Professional Driver";
        int jobYearsA = 2;
        int jobYearsB = 3;
        String jobDescription = "no Description";
        String email = "abcdefgh@abcdefgh.com";

        Employee employeeA = new Employee(firstNameA, lastNameA, descriptionA
                , jobDescription, jobYearsA, email);
        //Act
        int result = employeeA.getJobYears();
        //Assert
        assertNotEquals(result, jobYearsB);
    }

    @Test
    void setJobYears() {
        //Arrange
        //Employee A
        String firstNameA = "James";
        String lastNameA = "May";
        String descriptionA = "Professional Driver";
        int jobYearsA = 2;
        String jobDescription = "no Description";
        String email = "abcdefgh@abcdefgh.com";

        Employee employeeA = new Employee(firstNameA, lastNameA, descriptionA
                , jobDescription, jobYearsA, email);
        //Act
        employeeA.setJobYears(3);
        int result = employeeA.getJobYears();
        //Assert
        assertEquals(result, jobYearsA + 1);
    }

    @Test
    void getFirstName() {
        //Arrange
        //Employee A
        String firstNameA = "James";
        String lastNameA = "May";
        String descriptionA = "Professional Driver";
        int jobYearsA = 2;
        String firstNameB = "Jeremy";
        String jobDescription = "no Description";
        String email = "abcdefgh@abcdefgh.com";

        Employee employeeA = new Employee(firstNameA, lastNameA, descriptionA
                , jobDescription, jobYearsA, email);
        //Act
        String result = employeeA.getFirstName();
        //Assert
        assertEquals(result, firstNameA);
        assertNotEquals(result, firstNameB);
    }


    @Test
    void setFirstName() {
        //Arrange
        //Employee A
        String firstNameA = "James";
        String lastNameA = "May";
        String descriptionA = "Professional Driver";
        int jobYearsA = 2;
        String anotherName = "Another Name";
        String jobDescription = "no Description";
        String email = "abcdefgh@abcdefgh.com";

        Employee employeeA = new Employee(firstNameA, lastNameA, descriptionA
                , jobDescription, jobYearsA, email);
        //Act
        employeeA.setFirstName(anotherName);
        String result = employeeA.getFirstName();
        //Assert
        assertEquals(result, anotherName);
        assertNotEquals(result, firstNameA);
    }

    @Test
    void getLastName() {
        //Arrange
        //Employee A
        String firstNameA = "James";
        String lastNameA = "May";
        String descriptionA = "Professional Driver";
        int jobYearsA = 2;
        String lastNameB = "Clarkson";
        String jobDescription = "no Description";
        String email = "abcdefgh@abcdefgh.com";

        Employee employeeA = new Employee(firstNameA, lastNameA, descriptionA
                , jobDescription, jobYearsA, email);
        //Act
        String result = employeeA.getLastName();
        //Assert
        assertEquals(result, lastNameA);
        assertNotEquals(result, lastNameB);
    }

    @Test
    void setLastName() {
        //Arrange
        //Employee A
        String firstNameA = "James";
        String lastNameA = "May";
        String descriptionA = "Professional Driver";
        int jobYearsA = 2;
        String anotherName = "Another Name";
        String jobDescription = "no Description";
        String email = "abcdefgh@abcdefgh.com";

        Employee employeeA = new Employee(firstNameA, lastNameA, descriptionA
                , jobDescription, jobYearsA, email);
        //Act
        employeeA.setLastName(anotherName);
        String result = employeeA.getLastName();
        //Assert
        assertEquals(result, anotherName);
        assertNotEquals(result, lastNameA);
    }

    @Test
    void getDescription() {
        //Arrange
        //Employee A
        String firstNameA = "James";
        String lastNameA = "May";
        String descriptionA = "Professional Driver";
        int jobYearsA = 2;
        String descriptionB = "Television presenter";
        String jobDescription = "no Description";
        String email = "abcdefgh@abcdefgh.com";

        Employee employeeA = new Employee(firstNameA, lastNameA, descriptionA,
                jobDescription, jobYearsA, email);

        //Act
        String result = employeeA.getDescription();
        //Assert
        assertEquals(result, descriptionA);
        assertNotEquals(result, descriptionB);
    }

    @Test
    void setDescription() {
        //Arrange
        //Employee A
        String firstNameA = "James";
        String lastNameA = "May";
        String descriptionA = "The funniest";
        int jobYearsA = 2;
        String anotherDescription = "Another Description";
        String jobDescription = "no Description";
        String email = "abcdefgh@abcdefgh.com";

        Employee employeeA = new Employee(firstNameA, lastNameA, descriptionA
                , jobDescription, jobYearsA, email);

        //Act
        employeeA.setDescription(anotherDescription);
        String result = employeeA.getDescription();
        //Assert
        assertEquals(result, anotherDescription);
        assertNotEquals(result, descriptionA);
    }

    @Test
    void getJobDescription() {
        //Arrange
        //Employee A
        String firstNameA = "James";
        String lastNameA = "May";
        String descriptionA = "The funniest";
        String jobDescription = "no Description";
        int jobYearsA = 2;
        String email = "abcdefgh@abcdefgh.com";

        Employee employeeA = new Employee(firstNameA, lastNameA, descriptionA
                , jobDescription, jobYearsA, email);
        //Act
        String result = employeeA.getJobDescription();
        //Assert
        assertEquals(result, jobDescription);
    }

    @Test
    void setEmail() {
        //Arrange
        //Employee A
        String firstNameA = "James";
        String lastNameA = "May";
        String descriptionA = "The funniest";
        int jobYearsA = 2;
        String anotherEmail = "email@email.com";
        String jobDescription = "no Description";
        String email = "email@email.com";

        Employee employeeA = new Employee(firstNameA, lastNameA, descriptionA
                , jobDescription, jobYearsA, email);
        //Act
        employeeA.setEmail(anotherEmail);
        String result = employeeA.getEmail();
        //Assert
        assertEquals(result, anotherEmail);

    }

    @Test
    void getEmail() {
        //Arrange
        //Employee A
        String firstNameA = "James";
        String lastNameA = "May";
        String descriptionA = "The funniest";
        String jobDescription = "no Description";
        int jobYearsA = 2;
        String email = "abcdefgh@abcdefgh.com";

        Employee employeeA = new Employee(firstNameA, lastNameA, descriptionA
                , jobDescription, jobYearsA, email);

        //Act
        String result = employeeA.getJobDescription();
        //Assert
        assertEquals(result, jobDescription);
    }

    @Test
    void setJobDescription() {
        //Arrange
        //Employee A
        String firstNameA = "James";
        String lastNameA = "May";
        String descriptionA = "The funniest";
        String jobDescription = "no Description";
        int jobYearsA = 2;
        String anotherDescription = "Another Description";
        String email = "abcdefgh@abcdefgh.com";

        Employee employeeA = new Employee(firstNameA, lastNameA, descriptionA
                , jobDescription, jobYearsA, email);
        //Act
        employeeA.setJobDescription(anotherDescription);
        String result = employeeA.getJobDescription();
        //Assert
        assertEquals(result, anotherDescription);
        assertNotEquals(result, descriptionA);
    }

    @Test
    void testOverride_obj_vs_sameObj() {
        String firstNameA = "James";
        String lastNameA = "May";
        String descriptionA = "Professional Driver";
        int jobYearsA = 2;
        String jobDescription = "no Description";
        String email = "abcdefgh@abcdefgh.com";

        Employee employeeA = new Employee(firstNameA, lastNameA, descriptionA
                , jobDescription, jobYearsA, email);

        assertEquals(employeeA, employeeA);
        assertNotEquals(employeeA, null);

    }

    @Test
    void testOverride_DifferentFirstName() {
        String firstNameA = "James";
        String firstNameB = "Jack";
        String jobDescription = "no Description";
        String lastNameA = "May";
        String descriptionA = "Professional Driver";
        int jobYearsA = 2;
        String email = "something@isep.ipp.pt";

        Employee employeeA = new Employee(firstNameA, lastNameA, descriptionA
                , jobDescription, jobYearsA, email);
        Employee employeeB = new Employee(firstNameB, lastNameA, descriptionA
                , jobDescription, jobYearsA, email);

        assertNotEquals(employeeA, employeeB);
    }

    @Test
    void testOverride_DifferentLastName() {
        String firstNameA = "James";
        String lastNameA = "May";
        String lastNameB = "Jack";
        String descriptionA = "The funniest";
        int jobYearsA = 2;
        String jobDescription = "Professional Driver";
        String email = "abcdefgh@abcdefgh.com";

        Employee employeeA = new Employee(firstNameA, lastNameA, descriptionA
                , jobDescription, jobYearsA, email);
        Employee employeeB = new Employee(firstNameA, lastNameB, descriptionA
                , jobDescription, jobYearsA, email);

        assertNotEquals(employeeA, employeeB);
    }

    @Test
    void testOverride_DifferentDescriptionName() {
        String firstNameA = "James";
        String lastNameA = "May";
        String descriptionA = "Professional Driver";
        String descriptionB = "The funniest";
        int jobYearsA = 2;
        String jobDescription = "no Description";
        String email = "abcdefgh@abcdefgh.com";

        Employee employeeA = new Employee(firstNameA, lastNameA, descriptionA
                , jobDescription, jobYearsA, email);
        Employee employeeB = new Employee(firstNameA, lastNameA, descriptionB
                , jobDescription, jobYearsA, email);

        assertNotEquals(employeeA, employeeB);
    }

    @Test
    void testOverride_DifferentJobDescription() {
        String firstNameA = "James";
        String lastNameA = "May";
        String descriptionA = "Professional Driver";
        int jobYearsA = 2;
        String jobDescription = "no Description";
        String jobDescriptionB = "Description";
        String email = "abcdefgh@abcdefgh.com";

        Employee employeeA = new Employee(firstNameA, lastNameA, descriptionA
                , jobDescription, jobYearsA, email);
        Employee employeeB = new Employee(firstNameA, lastNameA, descriptionA
                , jobDescriptionB, jobYearsA, email);

        assertNotEquals(employeeA, employeeB);
    }

    @Test
    void testOverride_DifferentJobYears() {
        String firstNameA = "James";
        String lastNameA = "May";
        String descriptionA = "Professional Driver";
        int jobYearsA = 2;
        int jobYearsB = 4;
        String jobDescription = "no Description";
        String email = "abcdefgh@abcdefgh.com";

        Employee employeeA = new Employee(firstNameA, lastNameA, descriptionA
                , jobDescription, jobYearsA, email);
        Employee employeeB = new Employee(firstNameA, lastNameA, descriptionA
                , jobDescription, jobYearsB, email);

        assertNotEquals(employeeA, employeeB);
    }

    @Test
    void testOverride_Different_Email() {
        String firstNameA = "James";
        String lastNameA = "May";
        String descriptionA = "Professional Driver";
        int jobYearsA = 2;
        int jobYearsB = 4;
        String jobDescription = "no Description";
        String email = "abcdefgh@abcdefgh.com";
        String email2 = "abcdefgh@abcdefgh.com";

        Employee employeeA = new Employee(firstNameA, lastNameA, descriptionA
                , jobDescription, jobYearsA, email);
        Employee employeeB = new Employee(firstNameA, lastNameA, descriptionA
                , jobDescription, jobYearsB, email2);

        assertNotEquals(employeeA, employeeB);
    }

    @Test
    void testOverride_SameObj_hashCode() {
        String firstNameA = "James";
        String firstNameB = "Jeremy";
        String lastNameA = "May";
        int jobYearsA = 2;
        String descriptionA = "Professional Driver";
        String jobDescription = "no Description";
        String email = "abcdefgh@abcdefgh.com";

        Employee employeeA = new Employee(firstNameA, lastNameA, descriptionA
                , jobDescription, jobYearsA, email);
        Employee employeeB = new Employee(firstNameB, lastNameA, descriptionA
                , jobDescription, jobYearsA, email);

        assertEquals(employeeA.hashCode(), employeeA.hashCode());
        assertNotEquals(employeeA.hashCode(), employeeB.hashCode());
    }

    @Test
    void testToStringWithTwoEntries() {
        String firstNameA = "James";
        String lastNameA = "May";
        String descriptionA = "Professional Driver";
        int jobYearsA = 2;
        String jobDescription = "no Description";
        String email = "abcdefgh@abcdefgh.com";

        Employee employeeA = new Employee(firstNameA, lastNameA, descriptionA
                , jobDescription, jobYearsA, email);

        assertEquals(employeeA.toString(), "Employee{id=null, " +
                "firstName='James', lastName='May', description='Professional" +
                " Driver', jobDescription='no Description', jobYears='2', " +
                "email='abcdefgh@abcdefgh.com'}");
    }


}