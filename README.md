# Individual Repository for DevOps

This repository contains the files for all the class assignments of DevOps. 

The alternatives are in the directory of the relative class assignment. 

* [Class Assignment 1](https://bitbucket.org/1211777/devops-21-22-atb-1121777/src/master/CA1/)

* [Class Assignment 2 - Part 1](https://bitbucket.org/1211777/devops-21-22-atb-1211777/src/master/CA2/part1/)

* [Class Assignment 2 - Part 2](https://bitbucket.org/1211777/devops-21-22-atb-1211777/src/master/CA2/part2/)

* [Class Assignment 3 - Part 1](https://bitbucket.org/1211777/devops-21-22-atb-1211777/src/master/CA3/part1/)

* [Class Assignment 3 - Part 2](https://bitbucket.org/1211777/devops-21-22-atb-1211777/src/master/CA3/part2/)

* [Class Assignment 4 - Part 1](https://bitbucket.org/1211777/devops-21-22-atb-1211777/src/master/CA4/part1/)

* [Class Assignment 4 - Part 2](https://bitbucket.org/1211777/devops-21-22-atb-1211777/src/master/CA4/part2/)

* [Class Assignment 5 - Part 1](https://bitbucket.org/1211777/devops-21-22-atb-1211777/src/master/CA5/part1)

* [Class Assignment 5 - Part 2](https://bitbucket.org/1211777/devops-21-22-atb-1211777/src/master/CA5/part2)

---
##  Student identification

* **Name:** Luís Filipe Salgado Alves
* **E-mail:** 1211777@isep.ipp.pt
* **TeamGroup:** Grupo4
* **Class:** TurmaB
* **Course:** SWitCH | Re-Qualification Programme for IT

---