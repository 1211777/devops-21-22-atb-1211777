# Class Assignment 4 Part 2

The source code for this assignment is located in the folder
[CA4-part1](https://bitbucket.org/1211777/devops-21-22-atb-1121777/src/master/CA4/part2)

# Containers with docker

## 1. Analysis, Design and Implementation

This class assignment requires the creation of a docker container for run the
CA2 application and another to run the database. The schema followed he
was[docker-compose-spring-tut-demo](https://bitbucket.org/atb/docker-compose-spring-tut-demo)
 
---

## 2 - Container creation with docker compose

To start I downloaded the repository provided by the teacher and changed the
docker-compose.yml file and the web container dockerfile to run with my version
of the application.

Then run the command:

```shell
docker compose up
```

And the result was:

- The container we are running
  at: [http://localhost:8080/basic-0.0.1-SNAPSHOT/]()
- The container we are running
  at: [http://localhost:8080/basic-0.0.1-SNAPSHOT/h2-console]()

> To access the database it is necessary to place the link:
> [jdbc:h2:tcp://192.168.56.11:9092/./jpadb]()

The result:

![AppRunning](Images/AppRunning.png)

Now it is possible to add/consult/delete data through the h2 console.

---

## 2.1 - Tag the image and publish it in docker hub

For pushing the tag from local machine to docker hub I followed the next steps:

- First: Commit the container into the repository:

```shell
 docker commit [Image] [docker hub use]/[Image/Repository name][Tag]

 docker commit f1a9b7ee2fcb luissalgadoalves/ca4_part2_web:latest
```

- Second: push the container into the remote repository:

```shell
docker push [docker hub user]/[Image/Repository name]:[Tag]

docker push luissalgadoalves/ca4_part2_web:latest
```

![dockerHub](Images/dockerCommit.png)

The images are available at:

- [ca4_part2_web](https://hub.docker.com/r/luissalgadoalves/ca4_part2_web)
- [ca4_part2_db](https://hub.docker.com/r/luissalgadoalves/ca4_part2_db)

![dockerHub](Images/dockerHub.png)

---

## 2. Create  copy of the database file

To create a copy I need to run a shell inside the container of the db:

```shell
# enter in the db container:
docker exec -it 6588b09eebe3 bash
```

Then I run the comand to copy the file jpabd.mv.db:

```shell
# enter in the db container:
cp jpadb.mv.db /usr/src/data-backup
```

The result was:

![copyDataBaseFile](Images/copyDataBaseFile.png)

## 3. Commands used:

I used the commands above during the development of this task to remove
unused/failed containers:

```ruby
# stop all containers 
docker container stop $(docker container ls -a -q)
```

```ruby
# Delete all images/containners/networks not used to remove cache
docker system prune --all --force --volumes
```

![systemPrune](Images/systemPrune.png)

The result of this is:

![systemPruneResult](Images/systemPruneResult.png)

- Other relevant commands:

```shell
# list all the containers
docker ps
# list the images 
docker images
# change container states
docker pause/unpause/start/restart/stop/run/kill [container id]
# remove containers
docker rm [container ID]
docker rm [container ID]
# remove images
docker rmi [image]
# commit images to docker hub
 docker commit [Image][docker hub use]/[Image/Repository name][Tag]
# push images to docker hub
docker push [docker hub user]/[Image/Repository name][Tag]
```

---

## 4. Deploy app with heroku

To implement this task I first started to create a repository in bitbucket and
copy only the app files. This was required because my repository have space and
files that I will not use in this task. I also edit the app configurations to run web and data base in the same container.
The files used are available at:  [bitbucket-devops-heroku](https://bitbucket.org/1211777/devops-heroku/)
Then I follow the presented steps:

1: Creation of an account at  [heroku](https://devcenter.heroku.com)

2: Install heroku CLI [herokuCLI](https://devcenter.heroku.com/articles/heroku-cli)

3: log in into heroku and create a token to relate with docker login.

![loginToken](Images/heroku/loginToken.png)

4: Create an app directory with heroku

![create_app](Images/heroku/create_app.png)

5: Push the app into heroku

![herokuPush](Images/heroku/herokuPush.png)

6: Release the app and open the webpage

![releaseAndOpen](Images/heroku/releaseAndOpen.png)

7 : The app is supposed to run over
the [tut-basic-gradle-heroku](https://devops-heroku-switch.herokuapp.com/)

---

## 4.1. Step backs:


 - The app was not able to find the path:
The creation of the image run as expected in the CLI but when I go to the server it was not able to find the app path.
To Correct this, I changed the path over the dockerfile, to run in the root of the container, and solved this question.

![fail_dir](Images/heroku/fail_dir.png)


- When the first step passed, the heroku don't recognize the "EXPOSE 8080" in the dockerfile. So I created a file named "catalina-dynamic-port.sh" located in the same path as the dockerfile, with the purpose of make sure that the port opened is the 8080 as I wanted. This was found in a [reddit-post](https://www.reddit.com/r/Heroku/comments/oyqybs/eploying_a_tomcat_based_docker_image)  and then confirmed by a colleague of the class.  

The content of the file was:

```bash
#!/bin/bash

echo $PORT

sed -i 's/port="8080"/port="'${PORT}'"/' /usr/local/tomcat/conf/server.xml

/usr/local/tomcat/bin/catalina.sh run

```

Also added the lines to the dockerfile, to run the file created above.

```bash
ADD ./catalina-dynamic-port.sh /usr/local/tomcat/bin/catalina-dynamic-port.sh

RUN chmod a+x /usr/local/tomcat/bin/catalina-dynamic-port.sh

CMD ["catalina-dynamic-port.sh"]
```

- After resolving this problem I denoted another one. The app is running above the cache size permitted. So I added the following line to the "catalina-dynamic-port.sh" to restrict the RAM usage of the application.

```bash

export JAVA_OPTS="-Djava.awt.headless=true -server -Xms48m -Xmx512m -XX:MaxPermSize=512m"
like 1

```

The result is visible at:
- App running: [web](https://devops-heroku-switch.herokuapp.com/basic-0.0.1-SNAPSHOT/)

![web](Images/heroku/web.png)

- Data base: [database](https://devops-heroku-switch.herokuapp.com/basic-0.0.1-SNAPSHOT/h2-console)

![h2-console](Images/heroku/h2-console.png)



## 4.2 Commands used:

````shell
#see the token
heroku auth:token
#Login with the token
docker login --username=luissalgadoalves --password=$(heroku auth:token) registry.heroku.com
# Create a app
heroku create -a devops-heroku-switch
# Change stack from initial state to container
heroku stack:set container --app devops-heroku-switch
# Push the app to the heroku server
heroku container:push web -a devops-heroku-switch
# Release the container
heroku container:release web -a devops-heroku-switch
# Open the web page where the app was released
heroku open -a devops-heroku-switch
````

---

## 4.3 Overview docker vs heroku

- Heroku provides two ways for you to deploy your app with Docker:
_Container Registry_ allows you to deploy pre-built Docker images to Heroku. 
Build your Docker images with _heroku.yml_ for deployment to Heroku. 

![docker_heroku](Images/docker_heroku.png)

| Principal differences |                                                                                                                                                                                                                                                                |
|-----------------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Environment           | Heroku must run in its own cloud environment, while Docker can run in an environment like a laptop or a remote server                                                                                                                                          |  
| Flexibility           | Because Heroku applications can only be run in the Heroku environment, this may create issues with vendor lock-in. Docker's flexibility frees you from these concerns, with deployment options ranging from on-premises servers to even the Heroku PaaS itself |  
| Pricing               | Docker is free of charge. Meanwhile, each Heroku dyno have a monthly cost.                                                                                                                                                                                     | 

| Heroku features | Description                                                                                                                                                                       |
|-----------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| dynos           | are virtualized Linux containers that are the building blocks of Heroku applications                                                                                              |  
| buildpack       | is a config script for the build automation process, describing how a container image should be created                                                                           |  
| Cli             | is Heroku's tool for building and running Heroku apps from within the terminal                                                                                                    | 
| add-ons         | are tools and services for extending a Heroku application's functionality, such as data storage and processing, monitoring, or analytics. Some of them need a payed subscription. | 


## 5. Issues and tags

### Issues created

| Issue number | Class Assignment | Description                  |
|--------------|------------------|------------------------------|
| #12          | CA4 Part-2       |     Containers with docker   |

### Tag's

| Tag versions | Class Assignment | Description             |
|--------------|------------------|-------------------------|
| ca4-part2    | CA4 Part-2       | Final version CA4 Part2 |

## 6. Student identification

* **Name:** Luís Filipe Salgado Alves
* **E-mail:** 1211777@isep.ipp.pt
* **TeamGroup:** Grupo4
* **Class:** TurmaB
* **Course:** SWitCH | Re-Qualification Programme for IT

---