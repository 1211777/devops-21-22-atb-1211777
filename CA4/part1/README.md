# Class Assignment 4 Part 1

The source code for this assignment is located in the folder
[CA4-part1](https://bitbucket.org/1211777/devops-21-22-atb-1121777/src/master/CA4/part1)
# Containers with docker

## 1. Analysis, Design and Implementation


This class assignment requires the creation of a docker container to run the CA2 application. 
 
---
## 1.1 - Testing and introduction to Docker and docker files

To start I created the first tutorial available in docker desktop application.
For that I run in the command line:

```ruby
docker run -d -p 80:80 docker/getting-started
```
And the result was a container running in [localhost:80]() with a Getting started tutorial for Docker. 

![Getting started](Images/gettingStarted.png)

After reading and interpreted the tutorial I deleted the container and it's image. The commands con be seen at the topic ## 2. Used resources

---
## 1.2 - Creating a container with ubuntu image (following the class from DevOps )

```ruby
docker run --name my-ubuntu-container -i -t ubuntu /bin/bash
```

With this command I'm creating a container with the image ubuntu and starts a shell.

![ExampleClass](Images/ExampleClass.png)

- In the Docker Desktop is possible to se the downloaded image Ubuntu, and the container my-ubuntu-container. (images above)

![container_my_ubuntu](Images/container_my_ubuntu.png)
![ImagesDownloaded](Images/ImagesDownloaded.png)

---
## 1.3 - Creating a docker images and running containers using the chat application from CA2 (building the chat server "inside" the Dockerfile)

The dockerfile uploaded in the folder [gradel_basic_demo_docker](https://bitbucket.org/1211777/devops-21-22-atb-1121777/src/master/CA4/part1/gradel_basic_demo_docker)
is described all the steps and commands needed to run a container from a dockerfile.
After the creation of this file, just open a shell and type the command:

```ruby
# To build the image
docker build --no-cache -t gradle_basic_demo_docker .
# To run the container
docker run -p 59001:59001 -d gradle_basic_demo_docker
```
After this is necessary to tun the client on the local machine with the command: 

```ruby
./gradlew runClient
```

The result was: 

![chatRunning](Images/chatRunning.png)

---
## 1.4 - Creating a docker images and running containers using the chat application from CA2 (building the chat server in my host computer and copy the jar file "into" the Dockerfile)

The dockerfile uploaded in the folder [docker_build_chat_server_from_host](https://bitbucket.org/1211777/devops-21-22-atb-1121777/src/master/CA4/part1/docker_build_chatserver_from_host)
is described all the steps and commands needed to run a container from a dockerfile.
After the creation of this file, just open a shell and type the command:

```ruby
# To build the image
docker build --no-cache -t docker_copy_from_host .
# To run the container
docker run -d -p 59001:59001 --name docker_copy docker_copy_from_host .

# To run clients on host
java -cp jarfile/basic_demo-0.1.0.jar basic_demo.ChatClientApp localhost 59001
```

With this last command is possible to add multiple chat clients on host machine:

![chatRunning_withCopy](Images/chatRunning_withCopy.png)

---
## 1.5 - Tag the image and publish it in docker hub

For pushing the tag from local machine to docker hub I followed the nexte steps:

 - First: Commit the container into the repository:
 ```ruby
 docker commit [Image] [docker hub use]/[Image/Repository name][Tag]
 docker commit 3a3b36578e04 luissalgadoalves/docker_copy_from_host:1
 ```
  - Second: push the container into the remote repository:
 ```ruby
docker push luissalgadoalves/docker_copy_from_host:1
docker push [docker hub user]/[Image/Repository name]:[Tag]
  ```


  The images are available at: [docker hub repository ](https://hub.docker.com/u/luissalgadoalves)
  


![dockerHub](Images/dockerHub.png)

---
## 2. Commands used:

```ruby
# list all the containers
docker ps
# list the images 
docker images
# change container states
docker pause/unpause/start/restart/stop/run/kill [container id]
# remove containers
docker rm [container ID]
docker rm [container ID]
# remove images
docker rmi [image]
# commit images to docker hub
 docker commit [Image][docker hub use]/[Image/Repository name][Tag]
# push images to docker hub
docker push [docker hub user]/[Image/Repository name][Tag]
```


---
## 3. Issues and tags

### Issues created

| Issue number | Class Assignment | Description                  |
|--------------|------------------|------------------------------|
| #11          | CA4 Part-1       |     Containers with docker   |

### Tag's

| Tag versions | Class Assignment | Description             |
|--------------|------------------|-------------------------|
| ca4-part1    | CA4 Part-1       | Final version CA4 Part1 |

## 4. Student identification

* **Name:** Luís Filipe Salgado Alves
* **E-mail:** 1211777@isep.ipp.pt
* **TeamGroup:** Grupo4
* **Class:** TurmaB
* **Course:** SWitCH | Re-Qualification Programme for IT

---