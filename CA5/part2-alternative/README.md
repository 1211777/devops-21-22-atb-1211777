# Class Assignment 5 Part 2

The source code for this assignment is located in the folder
[CA5-part2](https://bitbucket.org/1211777/devops-21-22-atb-1121777/src/master/CA5/part2-alternative)
#  CI/CD Pipelines with Jenkins

## 1. Analysis, Design and Implementation

The main goal of this class assignment is to create a pipeline with jenkins using the gradle basic demo present in
[CA2-part1](https://bitbucket.org/1211777/devops-21-22-atb-1121777/src/master/CA5/part1)

As alternative will use Bitbucket to build the pipeline.

 
 ---
## 1.1 - Set up BitBucket and the main configurations:

To set-up the pipeline I used the tutorial from Bitbucket available at: [bitbucket Pipelines](https://support.atlassian.com/bitbucket-cloud/docs/get-started-with-bitbucket-pipelines/)

The first step is to create a file with YAML syntax named **bitbucket-pipelines.yml** and I follow the tutorial for this at: [bitbucket configure pipeline](https://support.atlassian.com/bitbucket-cloud/docs/configure-bitbucket-pipelinesyml/)

The main configurations are similar to jenkins and I will follow the same approach for stages.

---
## 2. Build of yaml file:

I followed the next steps to accomplish the alternative of the class assignment 5.

* Checkout from remote repository:
* Assemble the application:
* Run tests:
* Build javadoc:
* Archiving all results:
* Build and publish docker image

These steps are visible at the [bitbucket-pipelines.yaml](https://bitbucket.org/1211777/devops-21-22-atb-1121777/src/master/CA5/part2-alternative/bitbucket-pipelines.yaml)

To achieve this result, was necessary to add a Dockerfile into the folder of the application used an was used the same as CA5-part2.


**The result was:**

![docker_hub](./images/allTasks.png)

#### Docker repository available at: [docker_repository_CA5p2_alternative](https://hub.docker.com/r/luissalgadoalves/devops_ca5_p2_alternative)


![docker_hub](./images/docker_hub.png)

![files generated](./images/downloads.png)
---
## 3. Jenkins vs bitbucket pipelines:

Jenkins allows for much greater customization and is easier to manage via plugins. It also has a very user friendly application, unlike bitbucket, where some menus are sometimes not intuitive, even though it is possible to do almost everything from the configuration file.

The differences between jenkins and bitbucket are not very significant, one of the most obvious is the need to create passwords and global variables through the application, which will be used to store usernames and passwords. It is also necessary to pass between steps given in cache, using the tag: artifacts specifying the location of its storage.

![create global variables](./images/credentials.png)

There is no need to install plugins on bitbucket, however it is necessary to specify the image (must be available on dockerHub) where we will mount our application. In this case I chose a gradle image with a compatible version and with jdk-11 that I was using in the app.

![JDK-Fail](./images/fail_jdk.png)


---
## 4. Documents consulted:


[Build docker image at bb pipelines](https://support.atlassian.com/bitbucket-cloud/docs/build-and-push-a-docker-image-to-a-container-registry/)

[Variables and secrets bitbucket](https://support.atlassian.com/bitbucket-cloud/docs/variables-and-secrets/)

[General documentation for accomplish this class assignment with bitbucket pipelines](https://support.atlassian.com/bitbucket-cloud/docs/get-started-with-bitbucket-pipelines/)

---
## 5. Issues and tags

### Issues created

| Issue number | Class Assignment | Description                   |
|--------------|------------------|-------------------------------|
| #14          | CA5 Part-2       | CI/CD Pipelines with Jenkins  |

### Tag's

| Tag versions | Class Assignment | Description             |
|--------------|------------------|-------------------------|
| ca5-part2    | CA5 Part-2       | Final version CA5 Part2 |

## 6. Student identification

* **Name:** Luís Filipe Salgado Alves
* **E-mail:** 1211777@isep.ipp.pt
* **TeamGroup:** Grupo4
* **Class:** TurmaB
* **Course:** SWitCH | Re-Qualification Programme for IT

---