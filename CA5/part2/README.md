# Class Assignment 5 Part 2

The source code for this assignment is located in the folder
[CA5-part2](https://bitbucket.org/1211777/devops-21-22-atb-1121777/src/master/CA5/part2)
#  CI/CD Pipelines with Jenkins

## 1. Analysis, Design and Implementation

The main goal of this class assignment is to create a pipeline with jenkins using the gradle basic demo present in
[CA2-part1](https://bitbucket.org/1211777/devops-21-22-atb-1121777/src/master/CA5/part1)

 
 ---
## 1.1 - Set up jenkins and the main configurations:

Installation of plugins: 

 - **Javadoc Plugin** - Support jenkins to build Javadocs.</p>
 - **HTML publisher plugin** -  Support jenkins to publish HTML reports</p>
 - **JUnit plugin** - Allows jenkins to publish JUnit tests.</p>
 - **Docker pipeline** - Allow syntax and docker builds.</p>


![plugins_fail](./images/plugins.png)

---
## 2. Build of jenkins file:

#### Checkout from remote repository:
```groovy
        stage('Checkout') {
            steps {
                echo 'Checking out...'
                git url: 'https://bitbucket.org/1211777/devops-21-22-atb-1211777'
            }
        }
```
#### Assemble the application:
```groovy

        stage('Assemble'){
            steps {
                echo 'Assembling...'
                dir ("CA2/part2/react-and-spring-data-rest-basic") {
                    script {
                    if (isUnix()){
                        sh 'chmod +x gradlew'
                        sh './gradlew assemble'}
                    else
                        bat './gradlew assemble'
                    }
                }
            }
        }
```
#### Run tests:
```groovy

        stage('Test') {
            steps {
                echo 'testing...'
                dir ("CA2/part2/react-and-spring-data-rest-basic"){
                     script {
                    if (isUnix()){
                        sh 'chmod +x gradlew'
                        sh './gradlew test'
                        junit 'build/test-results/test/*.xml'}
                    else
                    {
                        bat './gradlew test'
                        junit 'build/test-results/test/*.xml'}
                    }
                }
            }
        }
```
#### Build javadoc:
```groovy

         stage('JavaDoc') {
            steps {
                echo 'documenting...'
                dir ("CA2/part2/react-and-spring-data-rest-basic"){
                    javadoc javadocDir: 'build/reports/tests/test', keepAll: false
                    publishHTML([allowMissing: false, alwaysLinkToLastBuild: false, keepAll: false,
                    reportDir: 'build/reports/tests/test', reportFiles: 'index.html',
                    reportName: 'HTML Report', reportTitles: 'Docs Loadtest Dashboard'])
                }
            }
        }
```
#### Archiving all results:
```groovy

        stage('Archiving') {
            steps {
                echo 'Archiving...'
                dir ("CA2/part2/react-and-spring-data-rest-basic"){
                archiveArtifacts 'build/libs/*'
                }
            }
        }
```
#### Build and publish docker image
```groovy

        stage ('Docker Image') {
            steps {
                script {
                    docker.withRegistry('','docker'){
                    def app =  docker.build("luissalgadoalves/docker-ca5-p2","ca2/part2/react-and-spring-data-rest-basic")
                    app.push()
                    }
                }
            }
        }
```

To achieve this result, was necessary to add a Dockerfile into the folder of the application used.

#### Dockerfile
```dockerfile
FROM tomcat:8-jdk8-temurin
COPY ./build/libs/react-and-spring-data-rest-basic-0.0.1-SNAPSHOT.war /usr/local/tomcat/webapps/
EXPOSE 8080
```



The result was:

![result](./images/Result.png)

Test results:

![test_Results](./images/test_Results.png)


![test_Results2](./images/test_Results2.png)

#### Docker repository available at: [docker_repository](https://hub.docker.com/r/luissalgadoalves/docker-ca5-p2)

![docker](./images/docker_repo.png)


---
## 3. Issues and tags

### Issues created

| Issue number | Class Assignment | Description                   |
|--------------|------------------|-------------------------------|
| #14          | CA5 Part-2       | CI/CD Pipelines with Jenkins  |

### Tag's

| Tag versions | Class Assignment | Description             |
|--------------|------------------|-------------------------|
| ca5-part2    | CA5 Part-2       | Final version CA5 Part2 |

## 4. Student identification

* **Name:** Luís Filipe Salgado Alves
* **E-mail:** 1211777@isep.ipp.pt
* **TeamGroup:** Grupo4
* **Class:** TurmaB
* **Course:** SWitCH | Re-Qualification Programme for IT

---