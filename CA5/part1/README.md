# Class Assignment 5 Part 1

The source code for this assignment is located in the folder
[CA5-part1](https://bitbucket.org/1211777/devops-21-22-atb-1121777/src/master/CA5/part1)
#  CI/CD Pipelines with Jenkins

## 1. Analysis, Design and Implementation

The main goal of this class assignment is to create a pipeline with jenkins using the gradle basic demo present in
[CA2-part1](https://bitbucket.org/1211777/devops-21-22-atb-1121777/src/master/CA2/part1)

 
 ---
## 1.1 - Set up jenkins and the main configurations:

I use the jenkins war file to run it in my computer.

The link for that is: [jenkins](https://www.jenkins.io/download/).

After downloading the file I used the command inside the folder with the file jenkins.war:
```shell
java -jar jenkins.war
```
This will deploy a server in localhost:8080 with jenkins.

Then, when assessing I followed the configurations from [jenkins-setup](https://www.jenkins.io/doc/book/installing/war-file/#setup-wizard), then create a new user and choosing the default plugins to install.


![plugins](../images/plugins.png)

After this jenkins is ready to start  !

---
## 1.2 - Deployment with the application in CA2-part1:
I created the jenkins file to run this application with the following stages:

```shell
#Stage CheckOut:
stage('Checkout') {
            steps {
                echo 'Checking out...'
                git url: 'https://bitbucket.org/1211777/devops-21-22-atb-1211777'
            }
        }
```

```shell
 #Stage Assemble:
        stage('Assemble') {
            steps {
                echo 'Assembling...'
                dir ("CA2/part1/gradle_basic_demo") {
                    script {
                    if (isUnix()){
                        sh 'chmod +x gradlew'
                        sh './gradlew assemble'}
                    else
                        bat './gradlew assemble'}
                }
            }
        }   
```

```shell
 #Stage Test:
        stage('Test') {
            steps {
                echo 'testing...'
                dir ("CA2/part1/gradle_basic_demo"){
                     script {
                    if (isUnix()){
                        sh 'chmod +x gradlew'
                        sh './gradlew test'}
                    else
                        bat './gradlew test'
                }

                }
            }
        }
```
```shell
 #Stage Archive:
        stage('Archiving') {
            steps {
                echo 'Archiving...'
                dir ("CA2/part1/gradle_basic_demo"){
                archiveArtifacts 'build/distributions/*'
                }
            }
        }
    
```

Note:
Is required to create an ssh key for jenkins to access bitbucket. I followed the steps required at
[bitbucket-sshKey](https://support.atlassian.com/bitbucket-cloud/docs/set-up-an-ssh-key/)
and used the public ssh key in bitbucket and the private ssh in jenkins. 
The difference is that the first stage need to bee like:

```shell
 #Stage CheckOut whit private repository:
stage('Checkout') {
            steps {
                echo 'Checking out...'
                git credentialsId: '1211777-bitbucket-credentials', url: 'https://bitbucket.org/1211777/devops-jenkins'
            }
        }
```

The complete file con bee seen ate this folder. 

After creating the file I started the deployment of my application:

- Create a new pipeline in jenkins:

![pipeline](../images/pipeline.png)

- Set initial configurations:

![pipeline](../images/pipeline_definition.png)

- First build:

![build](../images/last_build.png)

For this first build I made a copy of the console log and are available at this location.


---


---
## 2. Step Backs:

During the installation of jenkins some plugins failed to install. 

![plugins_fail](../images/plugins_fail.png)

After initiation of the jenkins, the following list of errors occurred and was necessary to install manually all the plugins in the image bellow.

![plugins_fail](../images/plugins_list.png)

```ruby
commands
```


---
## 3. Issues and tags

### Issues created

| Issue number | Class Assignment | Description                   |
|--------------|------------------|-------------------------------|
| #13          | CA5 Part-1       | CI/CD Pipelines with Jenkins  |

### Tag's

| Tag versions | Class Assignment | Description             |
|--------------|------------------|-------------------------|
| ca5-part1    | CA5 Part-1       | Final version CA5 Part1 |

## 4. Student identification

* **Name:** Luís Filipe Salgado Alves
* **E-mail:** 1211777@isep.ipp.pt
* **TeamGroup:** Grupo4
* **Class:** TurmaB
* **Course:** SWitCH | Re-Qualification Programme for IT

---