# Class Assignment 2 Part 1

The source code for this assignment is located in the folder
[CA2-part1](https://bitbucket.org/1211777/devops-21-22-atb-1121777/src/master/CA2/part1)

## 1. Analysis, Design and Implementation

To perform the requested tasks, it is necessary to edit the file

- build.gradle

#### Tasks:

* Task to run the server
``` groovy
task runServer(type: JavaExec, dependsOn: classes) {
group = "DevOps"
description = "Launches a server client that connects to a server on localhost:59001 "
    classpath = sourceSets.main.runtimeClasspath
    mainClass = 'basic_demo.ChatServerApp'
    args '59001'
}
```
Command Line
```bash  
./gradlew runServer
```



---
* Task to run tests
```groovy
test {
    useJUnit()
    maxHeapSize = '1G'
}
```
- In this task I explored moore and decided to add a new feature.
  Those lines add a closure to be notified after a test suite has executed.
  TestDescriptor and TestResult instance are passed to the closure.
```groovy
testLogging {
    afterSuite { desc, result ->
        if (!desc.parent) { // will match the outermost suite
            println "Results: " +
                    "${result.resultType} (${result.testCount} tests, " +
                    "${result.successfulTestCount} successes, " +
                    "${result.failedTestCount} failures, " +
                    "${result.skippedTestCount} skipped)"
        }
    }
}
```
- The result when the test is runned is the addiction of the following line:
```
Results: SUCCESS (1 tests, 1 successes, 0 failures, 0 skipped)
```
Command Line
```bash
./gradlew test
```
---


* Task to run Copy
```groovy
task copy(type: Copy) {
    from 'src'
    into 'backup'
}
```
Command Line
```bash
./gradlew copy
```


---
* Task to run Copy to a zip file
```groovy
task copyZIP (type: Zip){
    archiveFileName = "backup.zip"
    destinationDirectory = file('backup_zip')
    from 'src'
}
```
Command Line
```bash
./gradlew copyZIP
```
---

## 2.

## Issues created

| Issue number | Class Assignment | Description              |
|--------------|------------------|--------------------------|
| #6           | CA2 Part-1       | Build Tools with Gradle  |

## Tag's

| Tag versions | Class Assignment  | Description             |
|--------------|-------------------|-------------------------|
| ca2-part1    | CA2 Part-1        | Final version CA2 Part1 |

## 3. Student identification

* **Name:** Luís Filipe Salgado Alves
* **E-mail:** 1211777@isep.ipp.pt
* **TeamGroup:** Grupo4
* **Class:** TurmaB
* **Course:** SWitCH | Re-Qualification Programme for IT

---